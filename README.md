# SmartPLS_Win (x32/x64)

>  Goal: gen an extension on the license of SmartPLS from a professional free license

![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)
![Build Status](https://img.shields.io/github/release/pandao/editor.md.svg?branch=master)


# HWID
- read Wikipedia

## An extension files?
- created by me

included
`run_X64.exe / run_X32.exe`
`eclipssec.exe`
`SmartPLSCR.exe`


### How I have an extension files? 
- You must provide information about time "date-month-year" that you did activation key in the first time. 

### If I don't remember exactly. Is it possible to create extension files? 
- Impossible. Look forward HWID part 

## New features on extension files
- Easy update new version from SmartPLS release (manually by hand)
- Internet access without delay
- Running smooth
- Keep orgin file of SmartPLS
- No modify anything
- Without harming windows
- No virus

## Important note for Win (x32 and x64): 
- require `Administration privileges` to run an extension files
<img src="admin.png" alt="Administration privileges"/> 

## Installation

* [ ]  Step 1: Copy 3 files to installation directory SmartPLS (e.g. `C:\Program Files\SmartPLS 3`)

* [ ]  Step 2: Run `run_X64.exe / run_X32.exe` with `Administration privileges`

* [ ]  Step 3: Create shortcut of `SmartPLSCR` to desktop

* [ ]  Step 4: Run `SmartPLSCR` at desktop with `Administration privileges`

* [ ]  Step 5: Wating to SmartPLS run and show this message

> <img src="succed.png" alt="No indicatiors to show"/> 


# Development
Want to contribute? No. Thank you.

# Contact me
Email: `howie[at]tungdao.org`